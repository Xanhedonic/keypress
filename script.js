/**
 * Nuspaudus atitinkamą klaviatūros klavišą, persiduoda parametras i funkciją "lightButton()",
 * kuri suranda Elementą per id ir prie to paties klasės pavadinimo prideda "light", nes
 * "light" jau yra aprasyta styles.css kaip atskira klasė
 */
document.addEventListener('keydown', function(event) {
    //event.code == 'KeyW' ? alert('W') : null;
    event.code == 'KeyW' ? lightOn('W') : null;
    event.code == 'KeyS' ? lightOn('S') : null;
    event.code == 'KeyA' ? lightOn('A') : null;
    event.code == 'KeyD' ? lightOn('D') : null;

    if (event.code == 'Digit1') {
        lightTimed('Digit1');
        setTimeout(() => {
            lightOff('Digit1');
        }, 1000);
    }

    if (event.code == 'Digit2') {
        lightTimed('Digit2');
        setTimeout(() => {
            lightOff('Digit2');
        }, 2000);
    }

    if (event.code == 'Digit3') {
        lightTimed('Digit3');
        setTimeout(() => {
            lightOff('Digit3');
        }, 3000);
    }

})
// WASD, švies tol, kol bus nuspausti klavišai, (keyup - atleidus mygtuką išsijungia)
document.addEventListener('keyup', function(event) {
    event.code == 'KeyW' ? lightOff('W') : null;
    event.code == 'KeyS' ? lightOff('S') : null;
    event.code == 'KeyA' ? lightOff('A') : null;
    event.code == 'KeyD' ? lightOff('D') : null;
})

// Toggle ant W+E su if(){}
if (document.getElementById('WE').className == 'button') {
    runOnKeys(
        () => jungiklis('WE'),
        "KeyW",
        "KeyE"
    )
} 
// Cia neveikia sitas kazkodel, nuspaudus vėl W+E nebeišsijungia
if (document.getElementById('WE').className != 'button') {
    runOnKeys(
        () => lightOff('WE'),
        "KeyW",
        "KeyE"
    )
}

function runOnKeys(func, ...codes) {
    let pressed = new Set();

    document.addEventListener('keydown', function(event) {
      pressed.add(event.code);

      for (let code of codes) { // are all keys in the set?
        if (!pressed.has(code)) {
          return;
        }
      }
      pressed.clear();

      func();
    });

    document.addEventListener('keyup', function(event) {
      pressed.delete(event.code);
    });

}

function lightOn(kodas) {
    document.getElementById(kodas).className = 'button light';
}

function lightTimed(kodas) {
    document.getElementById(kodas).className = 'button timed';
}

function lightOff(kodas) {
    document.getElementById(kodas).className = 'button';
}

function jungiklis(kodas) {
    document.getElementById(kodas).className = 'button switch';
}